define(
    [
        'uiComponent',
        'ko',
        'Magento_Customer/js/customer-data',
    ], function (Component, ko, customerData) {
        'use strict';

        return Component.extend({
            defaults: {
                not_success_message: '',
                success_message: '',
                message: ko.observable('')
            },


            /**
             * @inheritdoc
             */
            initialize: function () {
                this._super();
                let cart = customerData.get('cart');

                cart.subscribe(this.priceCalculation, this);
                this.setMessage(this.not_success_message, this.total);
            },

            /**
             * @param items
             */
            priceCalculation: function (items) {
                let priceProductsSameBrand = 0;
                if (items.items.length === 0) {
                    this.setMessage(this.not_success_message, this.total);
                } else {
                    items.items.forEach(function (item) {
                        if (this.brand === item.brand) {
                            priceProductsSameBrand += item.product_price_value * item.qty;
                        }
                    }, this);

                    if (priceProductsSameBrand < this.total) {
                        this.setMessage(this.not_success_message, this.total - priceProductsSameBrand);
                    } else {
                        this.setMessage(this.success_message);
                    }
                }
            },

            setMessage: function (string, total) {
                this.message(string.replace(/%1/i, total));
            }
        });
    });
