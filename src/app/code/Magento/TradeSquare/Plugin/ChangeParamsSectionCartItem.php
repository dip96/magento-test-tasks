<?php

namespace Magento\TradeSquare\Plugin;

class ChangeParamsSectionCartItem
{
    /**
     * @param $subject
     * @param callable $proceed
     * @param $item
     *
     * @return mixed
     */
    public function aroundGetItemData($subject, callable $proceed, $item)
    {
        $product = $item->getProduct();
        $returnValue = $proceed($item);
        $returnValue['brand'] = $product->getBrand();
        return $returnValue;
    }
}
