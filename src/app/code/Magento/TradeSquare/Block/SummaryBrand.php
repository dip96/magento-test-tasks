<?php

namespace Magento\TradeSquare\Block;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View\AbstractView;

class SummaryBrand extends AbstractView
{
    private Json $serializer;

    /**
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param Json $serializer
     * @param array $data
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        Json $serializer,
        array $data = []
    ) {
        parent::__construct($context, $arrayUtils, $data);
        $this->serializer = $serializer;
    }

    /**
     * @return bool|string
     */
    public function getJsLayout()
    {
        $layout = $this->serializer->unserialize(parent::getJsLayout());
        $layout["components"]["summary_brand"]["brand"] = $this->getProduct()->getBrand();
        $layout["components"]["summary_brand"]["total"] = $this->_scopeConfig->getValue('catalog/product_page/min_sum_order_for_brand');
        $layout["components"]["summary_brand"]["not_success_message"] = __("Add %1 to cart to reach min order amount");
        $layout["components"]["summary_brand"]["success_message"] = __("Min order amount reached");
        return $this->serializer->serialize($layout);
    }
}
